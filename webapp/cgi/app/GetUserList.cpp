#include <webx/menu.h>

class GetUserList : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(GetUserList)

int GetUserList::process()
{
	param_int(pagenum);
	param_int(pagesize);

	checkLogin();

	sp<DBConnect> dbconn = webx::GetDBConnect();

	if (pagenum < 0 || pagesize < 1 || pagesize > 100) simpleResponse(XG_PARAMERR);

	param_string(user);
	param_string(group);

	if (user.length() > 64 || group.length() > 64) return simpleResponse(XG_PARAMERR);

	int res = 0;
	int num = 0;
	string cond;
	string sqlcmd;

	cond = "FROM T_XG_USER WHERE (USER LIKE '" + user + "%' OR NAME LIKE '" + user + "%') AND GROUPLIST LIKE '" + group + "%'";
	
	if (dbconn->select(num, "SELECT COUNT(USER) " + cond) < 0) return simpleResponse(XG_SYSERR);

	sqlcmd = "SELECT USER,NAME,LEVEL,GROUPLIST,ENABLED,LANGUAGE,MAIL,PHONE,CERTNO,ADDRESS,REMARK ";
	sqlcmd += cond + " ORDER BY USER ASC,STATETIME DESC" + webx::GetLimitString(dbconn.get(), pagesize, pagenum);

	res = webx::PackJson(dbconn->query(sqlcmd), "list", json);

	json["pagecount"] = (pagesize + num - 1) / pagesize;
	json["code"] = res;
	out << json;

	return XG_OK;
}